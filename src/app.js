'use strict';

var express = require('express');
var app = express();
app.set('trust proxy', true)

// bot
var bot = require('./bot');
bot.setup(app);

const PORT = 3978

app.post('/test', (req, res) => {
    res.send('ok')
})

app.listen(process.env.PORT || PORT, function() {
    console.log(`App started listening on port ${process.env.PORT || PORT}`);
});