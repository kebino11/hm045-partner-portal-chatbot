'use strict';

module.exports.setup = function(app) {
    var builder = require('botbuilder');
    var teams = require('botbuilder-teams');
    var config = require('config');
    var botConfig = config.get('bot');
    var request = require('request');
    const BASE_URL = 'http://68.183.239.211:3000/api';
    
    // Create a connector to handle the conversations
    var connector = new teams.TeamsChatConnector({
        appId: process.env.MICROSOFT_APP_ID || botConfig.microsoftAppId,
        appPassword: process.env.MICROSOFT_APP_PASSWORD || botConfig.microsoftAppPassword
    });
    
    var inMemoryStorage = new builder.MemoryBotStorage();
    var bot = new builder.UniversalBot(connector, function(session) {
        var text = teams.TeamsMessage.getTextWithoutMentions(session.message);

        if(text.toLowerCase().trim() === 'brands') {
            session.beginDialog('brands')
        }
        else {
            session.beginDialog('productsByBrand', text)
        }

    }).set("storage", inMemoryStorage);

    bot.dialog('brands', (session) => {
        session.send("Hello! I will be processing your request. This may take a while, please wait a moment.")
            request.get(
                BASE_URL + '/user',
                (err, res, body) => {

                    if(err) {
                        console.log(err);
                        session.endConversation("Sorry your request cannot be processed right now. Something went wrong in the server.");
                        return;
                    }
                    console.log(body)
                    let parsedData = JSON.parse(body);
                    let data = parsedData.map(user => user.username).join('\n')

                    session.endConversation(data);                           
                }
            )
    })

    bot.dialog('productsByBrand', (session, args) => {
        session.send("This may take a while, please wait a moment.")
        request.get(
            BASE_URL + `/product/brand/${args.replace(' ', '%20')}`,
            (err, res, body) => {

                if(err) {
                    console.log(err);
                    session.endConversation("Sorry your request cannot be processed right now. Something went wrong in the server.");
                    return;
                }
                console.log(body)
                let parsedData = JSON.parse(body);
                let data = parsedData.map(product => {
                    return `${product.name} - P${product.price}`
                }).join('\n')

                session.endConversation(data);                           
            }
        )
    })

    app.post('/api/messages', connector.listen());

    module.exports.connector = connector;
};
